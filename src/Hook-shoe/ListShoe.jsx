import { Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

function ListShoe({ dataShoeServer }) {
  let [dataShoe, setDataShoe] = useState([]);
  useEffect(() => {
    setDataShoe(dataShoeServer);
  }, []);
  let renderListShoe = () => {
    return dataShoe.map((shoe, index) => {
      return <ItemShoe shoe={shoe} key={index} />;
    });
  };
  return <Row>{renderListShoe()}</Row>;
}
const mapStateToProps = (state) => {
  return {
    dataShoeServer: state.shoeReducer.dataShoe,
  };
};
export default connect(mapStateToProps)(ListShoe);
