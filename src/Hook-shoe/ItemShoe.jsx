import { ShoppingCartOutlined } from "@ant-design/icons";
import { Col, Card } from "antd";
import React from "react";
import { connect } from "react-redux";
import { handleAddShoeAction } from "../redux/action/ShoeAction";
const { Meta } = Card;
function ItemShoe({ shoe, dispatch }) {
  return (
    <Col span={8} className="p-8">
      <Card
        hoverable
        style={{ width: "100%" }}
        cover={<img alt="example" src={shoe.image} />}
      >
        <Meta title={<p className="text-base"> {shoe.name}</p>} />
        <button
          onClick={() => {
            dispatch(handleAddShoeAction(shoe));
          }}
          className="bg-blue-500 rounded-lg py-2 px-4 text-white  flex items-center hover:bg-blue-800"
        >
          <ShoppingCartOutlined /> <span className="ml-2">Mua Hahaha</span>
        </button>
        <Meta />
      </Card>
    </Col>
  );
}
export default connect()(ItemShoe);
