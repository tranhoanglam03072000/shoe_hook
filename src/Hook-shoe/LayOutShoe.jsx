import React from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";

export default function LayOutShoe() {
  return (
    <div className="container mx-auto">
      <CartShoe />
      <ListShoe />
    </div>
  );
}
