import React, { useEffect, useState } from "react";
import { Button, Modal, Space, Table } from "antd";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import {
  handleBuyShoeAction,
  handleChangeQuantityAction,
  handleDeleteShoeAction,
} from "../redux/action/ShoeAction";
import { Alert } from "antd";
function CartShoe({ cartShoe, dispatch }) {
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const showModal = () => {
    setOpen(true);
  };
  const handleOk = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setOpen(false);
    }, 0);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Hình Ảnh",
      dataIndex: "image",
      key: "image",
      render: (url) => <img src={url} alt="" width={80} />,
    },
    {
      title: "Giá",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Số Lượng",
      key: "soLuong",
      dataIndex: "soLuong",
      render: (_, record) => {
        return (
          <Space>
            <button
              className="bg-green-500 hover:bg-green-700 text-white  px-4 py-2 rounded "
              onClick={() => {
                dispatch(handleChangeQuantityAction(record.key, 1));
              }}
            >
              +
            </button>
            <span>{record.soLuong}</span>
            <button
              className="bg-red-500 hover:bg-red-900 text-white  px-4 py-2 rounded"
              onClick={() => {
                dispatch(handleChangeQuantityAction(record.key, -1));
              }}
            >
              -
            </button>
          </Space>
        );
      },
    },
    {
      title: "Thành Tiền",
      key: "thanhTien",
      render: (_, record) => (
        <Space size="middle">
          <span>{record.soLuong * record.price} $</span>
        </Space>
      ),
    },
    {
      title: "Tuỳ Chọn",
      key: "tuyChon",
      render: (_, record) => (
        <Space size="middle">
          <button
            className="bg-red-600 text-white px-6 py-3 rounded"
            onClick={() => {
              dispatch(handleDeleteShoeAction(record.key));
            }}
          >
            Xoá
          </button>
        </Space>
      ),
    },
  ];
  let tongTien = () => {
    return cartShoe.reduce((tongTien, shoe) => {
      return tongTien + shoe.price * shoe.soLuong;
    }, 0);
  };
  return (
    <>
      <Button type="primary" onClick={showModal}>
        Giỏ Hàng <ShoppingCartOutlined />
      </Button>
      <Modal
        open={open}
        title="Giỏ Hàng"
        onOk={handleOk}
        width={1000}
        onCancel={handleCancel}
        footer={[
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={() => {
              dispatch(handleBuyShoeAction());
              handleOk();
            }}
          >
            Mua Hàng
          </Button>,
        ]}
      >
        <Table
          columns={columns}
          dataSource={cartShoe}
          pagination={{ pageSize: 4 }}
          scroll={{ y: 240 }}
        />
        <div className="text-3xl text-emerald-700">
          Tổng Tiền : {tongTien()} $
        </div>
      </Modal>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    cartShoe: state.shoeReducer.cartShoe,
  };
};
export default connect(mapStateToProps)(CartShoe);
