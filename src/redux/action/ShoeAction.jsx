import {
  ADD_SHOE,
  BUY_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
} from "../constant/ShoeConstant";

export const handleAddShoeAction = (shoe) => ({
  type: ADD_SHOE,
  payload: shoe,
});
export const handleDeleteShoeAction = (keyShoe) => ({
  type: DELETE_SHOE,
  payload: {
    keyShoe,
  },
});
export const handleChangeQuantityAction = (keyShoe, value) => ({
  type: CHANGE_QUANTITY,
  payload: {
    keyShoe,
    value,
  },
});
export const handleBuyShoeAction = () => ({
  type: BUY_SHOE,
});
