export const ADD_SHOE = "ADD_SHOE";
export const DELETE_SHOE = "DELETE_SHOE";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const BUY_SHOE = "BUY_SHOE";
