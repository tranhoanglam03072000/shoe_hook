import { dataShoe } from "../../data/dataShoe";
import {
  ADD_SHOE,
  BUY_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
} from "../constant/ShoeConstant";
import Swal from "sweetalert2";
const initialState = {
  dataShoe: dataShoe,
  cartShoe: [],
};

export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_SHOE: {
      let index = state.cartShoe.findIndex((shoe) => shoe.key == payload.key);
      if (index == -1) {
        state.cartShoe.push({ ...payload, soLuong: 1 });
      } else {
        state.cartShoe[index].soLuong++;
      }
      return { ...state, cartShoe: [...state.cartShoe] };
    }
    case DELETE_SHOE:
      {
        let newCart = state.cartShoe.filter(
          (shoe) => shoe.key != payload.keyShoe
        );

        return { ...state, cartShoe: newCart };
      }
      break;
    case CHANGE_QUANTITY:
      {
        console.log(payload);
        let index = state.cartShoe.findIndex(
          (shoe) => shoe.key == payload.keyShoe
        );
        if (index != -1) {
          state.cartShoe[index].soLuong += payload.value;
          if (state.cartShoe[index].soLuong <= 0) {
            state.cartShoe.splice(index, 1);
          }
        }
        return { ...state, cartShoe: [...state.cartShoe] };
      }
      break;
    case BUY_SHOE: {
      if (state.cartShoe.length == 0) {
        setTimeout(() => {
          Swal.fire({
            title: "",
            html: `<div>Thất bại là mẹ thành công</div>`,
            icon: "error",
            confirmButtonText: "Đóng",
          });
        }, 0);
      } else {
        state.cartShoe = [];
        setTimeout(() => {
          Swal.fire({
            title: "",
            html: `<div>mua hàng thành công</div>`,
            icon: "success",
            confirmButtonText: "Đóng",
          });
        }, 0);
      }
      return { ...state, cartShoe: [...state.cartShoe] };
    }
    default:
      return state;
  }
};
