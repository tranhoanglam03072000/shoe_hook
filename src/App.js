import logo from "./logo.svg";
import "./App.css";
import LayOutShoe from "./Hook-shoe/LayOutShoe";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import DemoTablee from "./demo/DemoTablee";
function App() {
  return (
    <div className="App">
      <LayOutShoe />
      {/* <DemoTablee /> */}
    </div>
  );
}

export default App;
